This module lets you use the resource labels in the DBpedia data set
(http://dbpedia.org), which is derived from the English Wikipedia,
as a web-based taxonomy in Drupal.

Since it works as a plugin for the Web Taxonomy module
(http://drupal.org/project/web_taxonomy), that module is required too.

The number of hits for the autocomplete list and a possible restriction
to a single DBpedia class can be configured in plugins/WtDbpedia.inc
(no UI currently).
